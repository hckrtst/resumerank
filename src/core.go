// Package rrcore provides apis that allow
// for providing a rank for a given résumé
// given the url for job Keywords
package rrcore

import (
	"errors"
	"regexp"

	"github.com/ledongthuc/pdf"
)

// Keywords represents the contents of the job Keywords
type Keywords []string

// Resume represents the contents of your resume
type Resume string

// Rank is the score that the resume gets when checked
// against the given Keywords
type Rank int

// extractResText extracts text from given resume
// in PDF format
func extractResText(path string) (string, error) {
	// check extension
	matched, err := regexp.Match(`.pdf$`, []byte(path))
	if matched == false {
		return "", errors.New("Unsupported extension")
	}
	// todo
	f, r, err := pdf.Open(path)
	defer f.Close()
	if err != nil {
		return "", errors.New("Failed to open " + path + ", err: " + err.Error())
	}

	_ = r
	return "", nil
}

// GetRank gets the rank of given resume
func GetRank(words Keywords, res Resume) Rank {
	var count Rank

	// we are assuming keywords are unique
	for _, word := range words {
		matched, err := regexp.Match(word, []byte(res))
		if err == nil && matched {
			count++
		}
	}
	return count
}
