// Package rrcore provides apis that allow

// for providing a rank for a given résumé

// given the url for job Keywords

package rrcore

import (
	"testing"
)

func TestGetRank(t *testing.T) {
	type args struct {
		p Keywords
		r Resume
	}
	tests := []struct {
		name string
		args args
		want Rank
	}{
		// TODO: Add test cases.
		{"base test", args{Keywords([]string{"abc", "qrs"}), Resume("def")}, 0},
		{"base test", args{Keywords([]string{"abc", "def"}), Resume("abc def")}, 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetRank(tt.args.p, tt.args.r); got != tt.want {
				t.Errorf("GetRank() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_extractResText(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
		{"valid extension", args{"../test_files/test.pdf"}, "", false},
		{"invalid extension", args{"test.doc"}, "", true},
		{"file cannot be opened", args{"nosuchfile.pdf"}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := extractResText(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("extractResText() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("extractResText() = %v, want %v", got, tt.want)
			}
		})
	}
}
