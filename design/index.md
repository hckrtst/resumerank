# Use case

* user provides local resume path and url for job posting
* we download posting and see how many relevant keywords are found in the resume
* filtering may be configurable 🤔
* based on the analysis we provide a score


# Goals

* simple to use
* efficient
* standalone core
* test driven
* multi-platform
* unicode support
